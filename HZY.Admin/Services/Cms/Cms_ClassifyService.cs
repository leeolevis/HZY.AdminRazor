﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Services.Cms
{
    using HZY.EFCore.Repository;
    using HZY.Models;
    using HZY.Models.Sys;
    using HZY.Toolkits;
    using HZY.EFCore.Base;
    using HZY.Admin.Services.Core;
    using HZY.EFCore;
    using HZY.Models.Cms;
    using HZY.Admin.Services.Sys;
    using HZY.Admin.Dto.Cms;
    using Microsoft.EntityFrameworkCore;

    public class Cms_ClassifyService : ServiceBase<Cms_Classify>
    {
        protected readonly AccountService accountService;

        public Cms_ClassifyService(
            EFCoreContext _db,
            AccountService _accountService)
            : base(_db)
        {
            this.accountService = _accountService;
        }


        #region CURD 基础

        /// <summary>
        ///  新增\修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<Guid> SaveAsync(Cms_ClassifyDto Dto)
        {
            var model = Dto.Model;
            model = await this.InsertOrUpdateAsync(model);
            return model.Classify_ID;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(List<Guid> Ids)
        {
            await this.DeleteAsync(w => Ids.Contains(w.Classify_ID));
            return 1;
        }

        /// <summary>
        /// 加载表单 数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> LoadFormAsync(Guid Id)
        {
            var res = new Dictionary<string, object>();

            var Model = await this.FindByIdAsync(Id);

            res[nameof(Id)] = Id == Guid.Empty ? "" : Id.ToString();
            res[nameof(Model)] = Model.NullSafe();

            return res;
        }

        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<TableViewModel> FindListAsync(int Page, int Rows, Cms_Classify Search)
        {
            var query = (
                from classify in db.Cms_Classifys
                from pmenu in db.Cms_Classifys.Where(w => w.Classify_ID == classify.Classify_ParentID).DefaultIfEmpty()
                select new { classify, 父级分类 = pmenu.Classify_Name }
                )
                .WhereIF(w => w.classify.Classify_ParentID == null || w.classify.Classify_ParentID == Guid.Empty, Search?.Classify_ParentID == Guid.Empty || Search?.Classify_ParentID == null)
                .WhereIF(w => w.classify.Classify_ParentID == Search.Classify_ParentID, Search?.Classify_ParentID != Guid.Empty && Search?.Classify_ParentID != null)
                .WhereIF(w => w.classify.Classify_Name.Contains(Search.Classify_Name), !string.IsNullOrWhiteSpace(Search?.Classify_Name))
                .OrderByDescending(w => w.classify.Classify_Num)
                .Select(w => new
                {
                    w.classify.Classify_Num,
                    w.classify.Classify_Type,
                    w.classify.Classify_Name,
                    w.classify.Classify_ParentID,
                    w.classify.Classify_Templete,
                    w.classify.Classify_Url,
                    w.classify.Classify_ArticleID,
                    w.父级分类,
                    w.classify.Classify_IsShow,
                    w.classify.Classify_IsDelete,
                    w.classify.Classify_CreateTime,
                    w.classify.Classify_ModifyTime,
                    _ukid = w.classify.Classify_ID
                })
                ;
            return await this.db.AsTableViewModelAsync(query, Page, Rows, typeof(Cms_Classify));
        }

        #endregion


        #region  创建系统左侧菜单

        /// <summary>
        /// 根据角色ID 获取菜单
        /// </summary>
        /// <returns></returns>
        public async Task<List<Cms_Classify>> GetMenuByRoleIDAsync()
        {
            var _Cms_ClassifyAllList = await this.Query()
                .Where(w => w.Classify_IsShow == 1)
                .OrderBy(w => w.Classify_CreateTime)
                .ToListAsync();

            if (this.accountService.info.IsSuperManage) return _Cms_ClassifyAllList;

            var _Cms_ClassifyList = await (
                    from roleMenuFunction in db.Sys_RoleMenuFunctions
                        //左连接 function
                    from function in db.Sys_Functions.Where(w => w.Function_ID == roleMenuFunction.RoleMenuFunction_FunctionID).DefaultIfEmpty()
                        //左连接 classify
                    from classify in db.Cms_Classifys.Where(w => w.Classify_ID == roleMenuFunction.RoleMenuFunction_MenuID).DefaultIfEmpty()
                    where this.accountService.info.RoleIDList.Contains(roleMenuFunction.RoleMenuFunction_RoleID) && function.Function_ByName == "Have"
                    select classify
                ).ToListAsync()
                ;

            var _New_Cms_ClassifyList = new List<Cms_Classify>();

            for (int i = 0; i < _Cms_ClassifyList.Count; i++)
            {
                var item = _Cms_ClassifyList[i];
                this.CheckUpperLevel(_Cms_ClassifyAllList, _Cms_ClassifyList, _New_Cms_ClassifyList, item);
                if (!_New_Cms_ClassifyList.Any(w => w.Classify_ID == item.Classify_ID)) _New_Cms_ClassifyList.Add(item);
            }

            return _New_Cms_ClassifyList.OrderBy(w => w.Classify_CreateTime).ToList();
        }

        private void CheckUpperLevel(List<Cms_Classify> Cms_ClassifyAllList, List<Cms_Classify> old_Cms_ClassifyList, List<Cms_Classify> new_Cms_ClassifyList, Cms_Classify classify)
        {
            if (!old_Cms_ClassifyList.Any(w => w.Classify_ID == classify.Classify_ParentID.ToGuid()) && !new_Cms_ClassifyList.Any(w => w.Classify_ID == classify.Classify_ParentID))
            {
                var _Menu = Cms_ClassifyAllList.Find(w => w.Classify_ID == classify.Classify_ParentID);
                if (_Menu != null)
                {
                    new_Cms_ClassifyList.Add(_Menu);
                    this.CheckUpperLevel(Cms_ClassifyAllList, old_Cms_ClassifyList, new_Cms_ClassifyList, _Menu);
                }
            }
        }

        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="_Cms_ClassifyList"></param>
        public List<Dictionary<string, object>> CreateMenus(Guid Id, List<Cms_Classify> _Cms_ClassifyList)
        {
            var _Menus = new List<Dictionary<string, object>>();
            List<Cms_Classify> _MenuItem = null;
            if (Id == Guid.Empty)
                _MenuItem = _Cms_ClassifyList.Where(w => w.Classify_ParentID == null || w.Classify_ParentID == Guid.Empty).ToList();
            else
                _MenuItem = _Cms_ClassifyList.Where(w => w.Classify_ParentID == Id).ToList();

            foreach (var item in _MenuItem)
            {
                _Menus.Add(new Dictionary<string, object>
                {
                    ["id"] = item.Classify_ID,
                    ["name"] = item.Classify_Name,
                    ["path"] = item.Classify_Url,
                    ["icon"] = "",
                    ["isClose"] = item.Classify_IsShow,
                    ["children"] = this.CreateMenus(item.Classify_ID.ToGuid(), _Cms_ClassifyList)
                });
            }
            return _Menus;
        }

        public async Task<Cms_Classify> GetMenuByIdAsync(Guid MenuId)
            => await this.FindAsync(w => w.Classify_ID == MenuId);

        #endregion  左侧菜单

        #region 创建菜单 功能 树

        public async Task<(List<object>, List<Guid>, List<string>)> GetMenuFunctionTreeAsync()
        {
            var _Cms_ClassifyList = await this.Query().OrderBy(w => w.Classify_CreateTime).ToListAsync();
            List<Guid> DefaultExpandedKeys = new List<Guid>();
            List<string> DefaultCheckedKeys = new List<string>();
            var tree = this.CreateClassifyTree(Guid.Empty, _Cms_ClassifyList, DefaultExpandedKeys, DefaultCheckedKeys);
            return (tree, DefaultExpandedKeys, DefaultCheckedKeys);
        }

        private List<object> CreateClassifyTree(Guid Id, List<Cms_Classify> _Cms_ClassifyList, List<Guid> _DefaultExpandedKeys, List<string> _DefaultCheckedKeys)
        {
            var _Menus = new List<object>();
            List<Cms_Classify> _MenuItem = null;
            if (Id == Guid.Empty)
                _MenuItem = _Cms_ClassifyList.Where(w => w.Classify_ParentID == null || w.Classify_ParentID == Guid.Empty).ToList();
            else
                _MenuItem = _Cms_ClassifyList.Where(w => w.Classify_ParentID == Id).ToList();

            foreach (var item in _MenuItem)
            {
                var _children = new List<object>();
                if (_Cms_ClassifyList.Any(w => w.Classify_ParentID == item.Classify_ID))
                {
                    _DefaultExpandedKeys.Add(item.Classify_ID);

                    _children = this.CreateClassifyTree(item.Classify_ID, _Cms_ClassifyList, _DefaultExpandedKeys, _DefaultCheckedKeys);
                }

                _Menus.Add(new
                {
                    key = item.Classify_ID,
                    title = $"{item.Classify_Name}",
                    disableCheckbox = true,
                    children = _children
                });
            }

            return _Menus;
        }

        #endregion
    }
}
