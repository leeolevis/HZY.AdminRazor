﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.Models.Sys
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Sys_AppLog))]
    public class Sys_AppLog
    {
        [Key]
        public Guid AppLog_ID { get; set; } = Guid.Empty;

        /// <summary>
        /// Api
        /// </summary>
        public string AppLog_Api { get; set; } = string.Empty;

        /// <summary>
        /// Ip
        /// </summary>
        public string AppLog_IP { get; set; } = string.Empty;

        /// <summary>
        /// UserID
        /// </summary>
        public Guid AppLog_UserID { get; set; } = Guid.Empty;

        /// <summary>
        /// 表单信息
        /// </summary>
        public string AppLog_Form { get; set; } = string.Empty;

        /// <summary>
        /// body 信息
        /// </summary>
        /// <value></value>
        public string AppLog_FormBody { get; set; } = string.Empty;

        /// <summary>
        /// 地址栏信息
        /// </summary>
        /// <value></value>
        public string AppLog_QueryString { get; set; } = string.Empty;

        /// <summary>
        /// 创建时间
        /// </summary>

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime AppLog_CreateTime { get; set; }


    }
}
