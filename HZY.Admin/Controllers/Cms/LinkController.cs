﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Controllers.Cms
{
    using HZY.Admin.Services.Cms;
    using HZY.Admin.Services.Sys;
    using HZY.Models.Cms;
    using HZY.Toolkits;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// Cms_Link管理
    /// </summary>
    public class LinkController : ApiBaseController<Cms_LinkService>
    {
        protected readonly IWebHostEnvironment webHostEnvironment;
        protected readonly string webRootPath;
        protected readonly AccountService accountService;
        public LinkController(Sys_MenuService _menuService, Cms_LinkService _service, IWebHostEnvironment _webHostEnvironment, AccountService _accountService)
            : base("08d84192-7ccf-4d69-8c8d-1d67ad58c7c9", _menuService, _service)
        {
            this.webHostEnvironment = _webHostEnvironment;
            this.webRootPath = _webHostEnvironment.WebRootPath;
            this.accountService = _accountService;
        }

        #region 页面 Views

        [HttpGet(nameof(Index))]
        public IActionResult Index() => View();

        [HttpGet("Info/{Id?}")]
        public IActionResult Info(Guid Id) => View(Id);

        #endregion

        #region 基础 CURD

        /// <summary>
        /// 查询数据列表
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        [HttpPost("FindList/{Page}/{Rows}")]
        public async Task<ApiResult> FindListAsync(int Page, int Rows, [FromBody] Cms_Link Search)
            => this.ResultOk(await this.service.FindListAsync(Page, Rows, Search));

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost("Save"), Core.HZYAppCheckModel]
        public async Task<ApiResult> SaveAsync([FromForm]Cms_Link Model)
        {
            IFormFile Photo = null;
            if (Request.Form.Files.Count > 0)
                Photo = Request.Form.Files.FirstOrDefault(w => w.Name == "Photo");

            //if (string.IsNullOrEmpty(Model.Link_Picture)) Model.Link_Picture = "";
            if (string.IsNullOrEmpty(Model.Link_Url)) Model.Link_Url = "";
            if (string.IsNullOrEmpty(Model.Link_Type)) Model.Link_Type = "";

            Model.Link_ModifyBy = this.accountService.info.UserName;
            return this.ResultOk(await this.service.SaveAsync(Model, this.webRootPath, Photo));
        }
        //=> this.ResultOk(await this.service.SaveAsync(Model));

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<ApiResult> DeleteAsync([FromBody]List<Guid> Ids)
            => this.ResultOk(await this.service.DeleteAsync(Ids));

        /// <summary>
        /// 根据Id 加载表单数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("LoadForm/{Id?}")]
        public async Task<ApiResult> LoadFormAsync(Guid Id)
            => this.ResultOk(await this.service.LoadFormAsync(Id));

        #endregion

        #region 导出 Excel

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="Search"></param>
        /// <returns></returns>
        [HttpPost("ExportExcel")]
        public async Task<FileContentResult> ExportExcel([FromBody] Cms_Link Search)
            => this.File(await this.service.ExportExcel(Search), Tools.GetFileContentType[".xls"].ToStr(), $"{Guid.NewGuid()}.xls");

        #endregion

    }
}