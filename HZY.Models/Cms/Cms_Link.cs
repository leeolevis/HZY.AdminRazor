﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.Models.Cms
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Cms_Link))]
    public class Cms_Link
    {
        [Key]
        public Guid Link_ID { get; set; } = Guid.Empty;

        [Required(ErrorMessage = "排序号不能为空!")]
        [RegularExpression(@"^[0-9]*$")]
        public int Link_Num { get; set; } = 1;

        /// <summary>
        /// 链接名称 => 备注:Link_Name 
        /// </summary>
        [Required(ErrorMessage = "链接名称不能为空!")]
        public string Link_Name { get; set; } = string.Empty;
        /// <summary>
        /// 链接地址 => 备注:Link_Url 
        /// </summary>
        [Url(ErrorMessage = "链接地址验证错误")]
        public string Link_Url { get; set; } = string.Empty;

        public string Link_Picture { get; set; } = string.Empty;

        /// <summary>
        /// 链接类型 => 备注:Link_Type 
        /// </summary>
        public string Link_Type { get; set; } = string.Empty;
        /// <summary>
        /// 是否显示 => 备注:Link_IsShow 
        /// </summary>
        public int Link_IsShow { get; set; } = 1;
        /// <summary>
        /// 是否可删除 => 备注:Link_IsDelete 
        /// </summary>
        public int Link_IsDelete { get; set; } = 2;
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Link_CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 修改时间 => 备注:Link_ModifyTime 
        /// </summary>
        public DateTime Link_ModifyTime { get; set; }
        /// <summary>
        /// 修改人 => 备注:Link_ModifyBy 
        /// </summary>
        public string Link_ModifyBy { get; set; } = string.Empty;
    }
}