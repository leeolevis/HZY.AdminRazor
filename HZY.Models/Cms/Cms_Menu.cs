﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.Models.Cms
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Cms_Menu))]
    public class Cms_Menu
    {
        [Key]
        public Guid Menu_ID { get; set; } = Guid.Empty;

        /// <summary>
        /// 请设置 Menu_Num 的显示名称 => 备注:Menu_Num
        /// </summary>
        [Required(ErrorMessage = "排序号不能为空!")]
        [RegularExpression(@"^[0-9]*$")]
        public int Menu_Num { get; set; } = 1;

        /// <summary>
        /// 请设置 Menu_Name 的显示名称 => 备注:Menu_Name
        /// </summary>
        [Required(ErrorMessage = "菜单名称不能为空!")]
        public string Menu_Name { get; set; } = string.Empty;

        [Required(ErrorMessage = "菜单类型不能为空!")]
        public string Menu_Type { get; set; } = string.Empty;

        /// <summary>
        /// 请设置 Menu_Url 的显示名称 => 备注:Menu_Url
        /// </summary>
        public string Menu_UrlID { get; set; } = string.Empty;

        public string Menu_UrlVal { get; set; } = string.Empty;

        /// <summary>
        /// 请设置 Menu_Icon 的显示名称 => 备注:Menu_Icon
        /// </summary>
        public string Menu_Icon { get; set; } = string.Empty;

        /// <summary>
        /// 请设置 Menu_ParentID 的显示名称 => 备注:Menu_ParentID
        /// </summary>
        public Guid Menu_ParentID { get; set; } = Guid.Empty;

        /// <summary>
        /// 是否显示 
        /// </summary>
        public int Menu_IsShow { get; set; } = 1;

        /// <summary>
        /// 关闭选项卡
        /// </summary>
        public int Menu_IsClose { get; set; } = 2;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Menu_CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 修改时间 => 备注:Menu_ModifyTime
        /// </summary>
        public DateTime Menu_ModifyTime { get; set; }

        /// <summary>
        /// 修改人 => 备注:Menu_ModifyBy
        /// </summary>
        public string Menu_ModifyBy { get; set; } = string.Empty;

    }
}