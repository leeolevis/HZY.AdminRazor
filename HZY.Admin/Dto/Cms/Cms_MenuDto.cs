﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Dto.Cms
{
    using HZY.Models.Cms;
    public class Cms_MenuDto
    {
        public Cms_Menu Model { get; set; }
    }
}
