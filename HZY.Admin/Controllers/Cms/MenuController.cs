﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HZY.Admin.Controllers.Cms
{
    using HZY.Admin.Dto.Cms;
    using HZY.Admin.Services.Cms;
    using HZY.Admin.Services.Sys;
    using HZY.Models.Cms;
    using HZY.Toolkits;
    using HZY.Toolkits.HzyNetCoreUtil.Attributes;

    /// <summary>
    /// 菜单管理
    /// </summary>
    public class MenuController : ApiBaseController<Cms_MenuService>
    {
        protected readonly AccountService accountService;

        public MenuController(Sys_MenuService _menuService, Cms_MenuService _service, AccountService _accountService)
            : base("08d83179-cb07-4651-8975-face49c9747c", _menuService, _service)
        {
            this.accountService = _accountService;
        }

        #region 页面 Views

        [HttpGet(nameof(Index))]
        public IActionResult Index() => View();

        [HttpGet("Info/{Id?}")]
        public IActionResult Info(Guid? Id, Guid? PId)
        {
            ViewData["PId"] = PId ?? Guid.Empty;
            return View(Id);
        }

        #endregion


        #region TREE

        /// <summary>
        /// 获取菜单功能树
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(MenuTree))]
        public async Task<ApiResult> MenuTree()
        {
            var menuFunctionTree = await this.service.GetMenuTreeAsync();

            return this.ResultOk(new
            {
                treeData = menuFunctionTree.Item1,
                defaultExpandedKeys = menuFunctionTree.Item2,
                defaultCheckedKeys = menuFunctionTree.Item3
            });
        }

        /// <summary>
        /// 查询数据列表
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        [HttpPost("FindList/{Page}/{Rows}")]
        public async Task<ApiResult> FindListAsync(int Page, int Rows, [FromBody] Cms_Menu Search)
            => this.ResultOk(await this.service.FindListAsync(Page, Rows, Search));

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <returns></returns>
        [AppTransaction]
        [HttpPost("Save"), Core.HZYAppCheckModel]
        public async Task<ApiResult> SaveAsync([FromBody]Cms_MenuDto Model)
        {
            Model.Model.Menu_ModifyBy = this.accountService.info.UserName;
            return this.ResultOk(await this.service.SaveAsync(Model)); ;
        }
            //=> this.ResultOk(await this.service.SaveAsync(Model));

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        [AppTransaction]
        [HttpPost("Delete")]
        public async Task<ApiResult> DeleteAsync([FromBody]List<Guid> Ids)
            => this.ResultOk(await this.service.DeleteAsync(Ids));

        /// <summary>
        /// 根据Id 加载表单数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("LoadForm/{Id?}")]
        public async Task<ApiResult> LoadFormAsync(Guid Id)
            => this.ResultOk(await this.service.LoadFormAsync(Id));

        #endregion

    }
}