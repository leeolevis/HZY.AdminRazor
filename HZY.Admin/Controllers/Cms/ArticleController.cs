﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Controllers.Cms
{
    using HZY.Admin.Services.Cms;
    using HZY.Admin.Services.Sys;
    using HZY.Models.Cms;
    using HZY.Toolkits;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// Cms_Article管理
    /// </summary>
    public class ArticleController : ApiBaseController<Cms_ArticleService>
    {
        protected readonly IWebHostEnvironment webHostEnvironment;
        protected readonly string webRootPath;
        protected readonly AccountService accountService;

        public ArticleController(Sys_MenuService _menuService, Cms_ArticleService _service, IWebHostEnvironment _webHostEnvironment, AccountService _accountService)
            : base("08d83ba0-ea7b-4f22-83be-c71f439cb572", _menuService, _service)
        {
            this.webHostEnvironment = _webHostEnvironment;
            this.webRootPath = _webHostEnvironment.WebRootPath;
            this.accountService = _accountService;
        }

        #region 页面 Views

        [HttpGet(nameof(Index))]
        public IActionResult Index() => View();

        [HttpGet("Info/{Id?}")]
        public IActionResult Info(Guid? Id, Guid? PId)
        {
            ViewData["PId"] = PId ?? Guid.Empty;
            return View(Id);
        }

        [HttpGet("Load/{Id?}")]
        public IActionResult Load(Guid? Id) => View(Id);

        [HttpGet(nameof(System))]
        public IActionResult System() => View();
        #endregion

        #region 基础 CURD

        /// <summary>
        /// 查询数据列表
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        [HttpPost("FindList/{Page}/{Rows}")]
        public async Task<ApiResult> FindListAsync(int Page, int Rows, [FromBody] Cms_Article Search)
        {
            var result = await this.service.FindListAsync(Page, Rows, Search);
            return this.ResultOk(result);
        }
        //=> this.ResultOk(await this.service.FindListAsync(Page, Rows, Search));

        
        [HttpPost("SystemList/{Page}/{Rows}")]
        public async Task<ApiResult> SystemListAsync(int Page, int Rows, [FromBody] Cms_Article Search)
        {
            var result = await this.service.SystemListAsync(Page, Rows, Search);
            return this.ResultOk(result);
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost("Save"), Core.HZYAppCheckModel]
        public async Task<ApiResult> SaveAsync([FromForm]Cms_Article Model)
        {
            IFormFile Photo = null;
            IFormFile Document = null;
            if (Request.Form.Files.Count > 0)
            {
                Photo = Request.Form.Files.FirstOrDefault(w => w.Name == "Photo");
                Document = Request.Form.Files.FirstOrDefault(w => w.Name == "Docs");
            }

            if (string.IsNullOrEmpty(Model.Article_SubTitle)) Model.Article_SubTitle = "";
            //if (string.IsNullOrEmpty(Model.Article_Picture)) Model.Article_Picture = "";
            if (string.IsNullOrEmpty(Model.Article_Document)) Model.Article_Document = "";
            if (string.IsNullOrEmpty(Model.Article_Tag)) Model.Article_Tag = "";
            if (string.IsNullOrEmpty(Model.Article_Describe)) Model.Article_Describe = "";
            if (string.IsNullOrEmpty(Model.Article_Author)) Model.Article_Author = "";
            if (string.IsNullOrEmpty(Model.Article_Url)) Model.Article_Url = "";
            if (string.IsNullOrEmpty(Model.Article_Content)) Model.Article_Content = "";
            if (!Model.Article_ClassID.HasValue) Model.Article_ClassID = Guid.Empty;

            Model.Article_ModifyBy = this.accountService.info.UserName;
            return this.ResultOk(await this.service.SaveAsync(Model, this.webRootPath, Photo, Document));
        }

        [HttpPost("SaveSystem"), Core.HZYAppCheckModel]
        public async Task<ApiResult> SaveSystemAsync([FromForm]Cms_Article Model)
        {
            IFormFile Photo = null;
            IFormFile Document = null;
            if (Request.Form.Files.Count > 0)
            {
                Photo = Request.Form.Files.FirstOrDefault(w => w.Name == "Photo");
                Document = Request.Form.Files.FirstOrDefault(w => w.Name == "Docs");
            }

            if (string.IsNullOrEmpty(Model.Article_SubTitle)) Model.Article_SubTitle = "";
            //if (string.IsNullOrEmpty(Model.Article_Picture)) Model.Article_Picture = "";
            if (string.IsNullOrEmpty(Model.Article_Document)) Model.Article_Document = "";
            if (string.IsNullOrEmpty(Model.Article_Tag)) Model.Article_Tag = "";
            if (string.IsNullOrEmpty(Model.Article_Describe)) Model.Article_Describe = "";
            if (string.IsNullOrEmpty(Model.Article_Author)) Model.Article_Author = "";
            if (string.IsNullOrEmpty(Model.Article_Url)) Model.Article_Url = "";
            if (string.IsNullOrEmpty(Model.Article_Content)) Model.Article_Content = "";
            if (string.IsNullOrEmpty(Model.Article_Content)) Model.Article_Content = "";

            Model.Article_IsSystem = 1;//设置位系统内容
            Model.Article_ModifyBy = this.accountService.info.UserName;
            return this.ResultOk(await this.service.SaveAsync(Model, this.webRootPath, Photo, Document));
        }

        //=> this.ResultOk(await this.service.SaveAsync(Model));

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<ApiResult> DeleteAsync([FromBody]List<Guid> Ids)
            => this.ResultOk(await this.service.DeleteAsync(Ids));

        /// <summary>
        /// 根据Id 加载表单数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("LoadForm/{Id?}")]
        public async Task<ApiResult> LoadFormAsync(Guid Id)
            => this.ResultOk(await this.service.LoadFormAsync(Id));


        [HttpPost("LoadInfo/{Id?}")]
        public async Task<ApiResult> LoadInfoAsync(Guid Id)
        {
            var model = await this.service.FindByIdAsync(Id);
            if (model == null)
            {
                model = new Cms_Article()
                {
                    Article_ID = Id,
                    Article_Title = string.Empty,
                    Article_IsSystem = 1
                };
                await this.service.SaveAsync(model, this.webRootPath);
            }

            return this.ResultOk(await this.service.LoadFormAsync(Id));
        }

        #endregion

        #region 导出 Excel

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="Search"></param>
        /// <returns></returns>
        [HttpPost("ExportExcel")]
        public async Task<FileContentResult> ExportExcel([FromBody] Cms_Article Search)
            => this.File(await this.service.ExportExcel(Search), Tools.GetFileContentType[".xls"].ToStr(), $"{Guid.NewGuid()}.xls");

        #endregion

    }
}