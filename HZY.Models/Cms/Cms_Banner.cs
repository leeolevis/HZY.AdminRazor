﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.Models.Cms
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Cms_Banner))]
    public class Cms_Banner
    {
        [Key]
        public Guid Banner_ID { get; set; } = Guid.Empty;
        /// <summary>
        /// 请设置 Banner_Num 的显示名称 => 备注:Banner_Num 
        /// </summary>
        [Required(ErrorMessage = "排序号不能为空!")]
        [RegularExpression(@"^[0-9]*$")]
        public int Banner_Num { get; set; } = 1;
        /// <summary>
        /// 请设置 Banner_Name 的显示名称 => 备注:Banner_Name 
        /// </summary>
        [Required(ErrorMessage = "Banner名称不能为空!")]
        public string Banner_Name { get; set; } = string.Empty;
        /// <summary>
        /// 菜单类别 => 备注:Banner_Type 
        /// </summary>
        public string Banner_Type { get; set; } = string.Empty;

        public string Banner_Picture { get; set; } = string.Empty;
        /// <summary>
        /// 请设置 Banner_UrlID 的显示名称 => 备注:Banner_UrlID 
        /// </summary>
        public string Banner_UrlID { get; set; }
        /// <summary>
        /// 请设置 Banner_UrlVal 的显示名称 => 备注:Banner_UrlVal 
        /// </summary>
        public string Banner_UrlVal { get; set; } = string.Empty;
        /// <summary>
        /// 请设置 Banner_IsShow 的显示名称 => 备注:Banner_IsShow 
        /// </summary>
        public int Banner_IsShow { get; set; } = 1;
        /// <summary>
        /// 请设置 Banner_IsDelete 的显示名称 => 备注:Banner_IsDelete 
        /// </summary>
        public int Banner_IsDelete { get; set; } = 2;
        /// <summary>
        /// 创建时间
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Banner_CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 修改时间 => 备注:Banner_ModifyTime 
        /// </summary>
        public DateTime Banner_ModifyTime { get; set; }
        /// <summary>
        /// 修改人 => 备注:Banner_ModifyBy 
        /// </summary>
        public string Banner_ModifyBy { get; set; } = string.Empty;

    }
}