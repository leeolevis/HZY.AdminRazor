﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HZY.Admin.Services.Cms
{
    using HZY.Admin.Core;
    using HZY.Admin.Dto.Cms;
    using HZY.Admin.Services.Core;
    using HZY.Admin.Services.Sys;
    using HZY.EFCore;
    using HZY.EFCore.Base;
    using HZY.EFCore.Repository;
    using HZY.Models.Cms;
    using HZY.Toolkits;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;
    using System.Linq;
    using System.Threading.Tasks;

    public class Cms_MenuService : ServiceBase<Cms_Menu>
    {
        protected readonly AccountService accountService;

        public Cms_MenuService(
            EFCoreContext _db,
            AccountService _accountService)
            : base(_db)
        {
            this.accountService = _accountService;
        }


        #region CURD 基础

        /// <summary>
        ///  新增\修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<Guid> SaveAsync(Cms_MenuDto Dto)
        {
            var model = Dto.Model;
            model = await this.InsertOrUpdateAsync(model);
            return model.Menu_ID;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(List<Guid> Ids)
        {
            await this.DeleteAsync(w => Ids.Contains(w.Menu_ID));
            return 1;
        }

        /// <summary>
        /// 加载表单 数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> LoadFormAsync(Guid Id)
        {
            var res = new Dictionary<string, object>();

            var Model = await this.FindByIdAsync(Id);

            res[nameof(Id)] = Id == Guid.Empty ? "" : Id.ToString();
            res[nameof(Model)] = Model.NullSafe();

            return res;
        }

        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<TableViewModel> FindListAsync(int Page, int Rows, Cms_Menu Search)
        {
            var query = (
                from menu in db.Cms_Menus
                from pmenu in db.Cms_Menus.Where(w => w.Menu_ID == menu.Menu_ParentID).DefaultIfEmpty()
                select new { menu, 父级菜单 = pmenu.Menu_Name }
                )
                .WhereIF(w => w.menu.Menu_ParentID == null || w.menu.Menu_ParentID == Guid.Empty, Search?.Menu_ParentID == Guid.Empty || Search?.Menu_ParentID == null)
                .WhereIF(w => w.menu.Menu_ParentID == Search.Menu_ParentID, Search?.Menu_ParentID != Guid.Empty && Search?.Menu_ParentID != null)
                .WhereIF(w => w.menu.Menu_Name.Contains(Search.Menu_Name), !string.IsNullOrWhiteSpace(Search?.Menu_Name))
                .OrderByDescending(w => w.menu.Menu_Num)
                .Select(w => new
                {
                    w.menu.Menu_Num,
                    w.menu.Menu_Name,
                    w.menu.Menu_Type,
                    w.menu.Menu_UrlID,
                    w.menu.Menu_UrlVal,
                    w.父级菜单,
                    w.menu.Menu_Icon,
                    w.menu.Menu_IsShow,
                    w.menu.Menu_CreateTime,
                    w.menu.Menu_ModifyTime,
                    _ukid = w.menu.Menu_ID
                })
                ;
            return await this.db.AsTableViewModelAsync(query, Page, Rows, typeof(Cms_Menu));
        }

        #endregion


        #region  创建系统左侧菜单

        /// <summary>
        /// 根据角色ID 获取菜单
        /// </summary>
        /// <returns></returns>
        public async Task<List<Cms_Menu>> GetMenuByRoleIDAsync()
        {
            var _Cms_MenuAllList = await this.Query()
                .Where(w => w.Menu_IsShow == 1)
                .OrderBy(w => w.Menu_Num)
                .ToListAsync();

            if (this.accountService.info.IsSuperManage) return _Cms_MenuAllList;

            var _Cms_MenuList = await (
                    from roleMenuFunction in db.Sys_RoleMenuFunctions
                        //左连接 function
                    from function in db.Sys_Functions.Where(w => w.Function_ID == roleMenuFunction.RoleMenuFunction_FunctionID).DefaultIfEmpty()
                        //左连接 menu
                    from menu in db.Cms_Menus.Where(w => w.Menu_ID == roleMenuFunction.RoleMenuFunction_MenuID).DefaultIfEmpty()
                    where this.accountService.info.RoleIDList.Contains(roleMenuFunction.RoleMenuFunction_RoleID) && function.Function_ByName == "Have"
                    select menu
                ).ToListAsync()
                ;

            var _New_Cms_MenuList = new List<Cms_Menu>();

            for (int i = 0; i < _Cms_MenuList.Count; i++)
            {
                var item = _Cms_MenuList[i];
                this.CheckUpperLevel(_Cms_MenuAllList, _Cms_MenuList, _New_Cms_MenuList, item);
                if (!_New_Cms_MenuList.Any(w => w.Menu_ID == item.Menu_ID)) _New_Cms_MenuList.Add(item);
            }

            return _New_Cms_MenuList.OrderBy(w => w.Menu_Num).ToList();
        }

        private void CheckUpperLevel(List<Cms_Menu> Cms_MenuAllList, List<Cms_Menu> old_Cms_MenuList, List<Cms_Menu> new_Cms_MenuList, Cms_Menu menu)
        {
            if (!old_Cms_MenuList.Any(w => w.Menu_ID == menu.Menu_ParentID.ToGuid()) && !new_Cms_MenuList.Any(w => w.Menu_ID == menu.Menu_ParentID))
            {
                var _Menu = Cms_MenuAllList.Find(w => w.Menu_ID == menu.Menu_ParentID);
                if (_Menu != null)
                {
                    new_Cms_MenuList.Add(_Menu);
                    this.CheckUpperLevel(Cms_MenuAllList, old_Cms_MenuList, new_Cms_MenuList, _Menu);
                }
            }
        }

        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="_Cms_MenuList"></param>
        public List<Dictionary<string, object>> CreateMenus(Guid Id, List<Cms_Menu> _Cms_MenuList)
        {
            var _Menus = new List<Dictionary<string, object>>();
            List<Cms_Menu> _MenuItem = null;
            if (Id == Guid.Empty)
                _MenuItem = _Cms_MenuList.Where(w => w.Menu_ParentID == null || w.Menu_ParentID == Guid.Empty).ToList();
            else
                _MenuItem = _Cms_MenuList.Where(w => w.Menu_ParentID == Id).ToList();

            foreach (var item in _MenuItem)
            {
                _Menus.Add(new Dictionary<string, object>
                {
                    ["id"] = item.Menu_ID,
                    ["name"] = item.Menu_Name,
                    ["path"] = item.Menu_UrlID,
                    ["icon"] = item.Menu_Icon,
                    ["isClose"] = item.Menu_IsClose,
                    ["children"] = this.CreateMenus(item.Menu_ID.ToGuid(), _Cms_MenuList)
                });
            }
            return _Menus;
        }

        public async Task<Cms_Menu> GetMenuByIdAsync(Guid MenuId)
            => await this.FindAsync(w => w.Menu_ID == MenuId);

        #endregion  左侧菜单

        #region 创建菜单 功能 树

        public async Task<(List<object>, List<Guid>, List<string>)> GetMenuTreeAsync()
        {
            var _Cms_MenuList = await this.Query().OrderBy(w => w.Menu_Num).ToListAsync();
            List<Guid> DefaultExpandedKeys = new List<Guid>();
            List<string> DefaultCheckedKeys = new List<string>();
            var tree = this.CreateMenuTree(Guid.Empty, _Cms_MenuList, DefaultExpandedKeys, DefaultCheckedKeys);
            return (tree, DefaultExpandedKeys, DefaultCheckedKeys);
        }

        private List<object> CreateMenuTree(Guid Id, List<Cms_Menu> _Cms_MenuList, List<Guid> _DefaultExpandedKeys, List<string> _DefaultCheckedKeys)
        {
            var _Menus = new List<object>();
            List<Cms_Menu> _MenuItem = null;
            if (Id == Guid.Empty)
                _MenuItem = _Cms_MenuList.Where(w => w.Menu_ParentID == null || w.Menu_ParentID == Guid.Empty).ToList();
            else
                _MenuItem = _Cms_MenuList.Where(w => w.Menu_ParentID == Id).ToList();

            foreach (var item in _MenuItem)
            {
                var _children = new List<object>();
                if (_Cms_MenuList.Any(w => w.Menu_ParentID == item.Menu_ID))
                {
                    _DefaultExpandedKeys.Add(item.Menu_ID);

                    _children = this.CreateMenuTree(item.Menu_ID, _Cms_MenuList, _DefaultExpandedKeys, _DefaultCheckedKeys);
                }

                _Menus.Add(new
                {
                    key = item.Menu_ID,
                    title = item.Menu_Name,//$"{item.Menu_Name}-{item.Menu_Num}",
                    disableCheckbox = true,
                    children = _children
                }); ;
            }

            return _Menus;
        }

        #endregion
    }
}
