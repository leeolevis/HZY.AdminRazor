﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Services.Cms
{
    using System.Threading.Tasks;
    using HZY.EFCore.Repository;
    using HZY.Models;
    using HZY.Models.Sys;
    using HZY.Toolkits;
    using HZY.EFCore.Base;
    using HZY.Admin.Services.Core;
    using HZY.EFCore;
    using HZY.Models.Cms;
    using Microsoft.AspNetCore.Http;

    public class Cms_LinkService : ServiceBase<Cms_Link>
    {
        public Cms_LinkService(EFCoreContext _db)
            : base(_db)
        {

        }

        #region CURD 基础

        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<TableViewModel> FindListAsync(int Page, int Rows, Cms_Link Search)
        {
            var query = this.Query()
                .WhereIF(w => w.Link_Name.Contains(Search.Link_Name), !string.IsNullOrWhiteSpace(Search?.Link_Name))
                .OrderByDescending(w => w.Link_Num)
                .Select(w => new
                {
                    w.Link_Num,
                    w.Link_Name,
                    w.Link_Url,
                    w.Link_Type,
                    w.Link_Picture,
                    w.Link_IsShow,
                    w.Link_IsDelete,
                    w.Link_CreateTime,
                    w.Link_ModifyTime,
                    w.Link_ModifyBy,
                    _ukid = w.Link_ID
                })
                ;

            return await this.db.AsTableViewModelAsync(query, Page, Rows, typeof(Cms_Link));
        }

        /// <summary>
        /// 新增\修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> SaveAsync(Cms_Link model, string webRootPath, IFormFile Photo)
        {
            if (Photo != null)
                model.Link_Picture = this.HandleUploadImageFile(Photo, webRootPath);

            await this.InsertOrUpdateAsync(model);

            return model.Link_ID;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(List<Guid> Ids)
            => await this.DeleteAsync(w => Ids.Contains(w.Link_ID));

        /// <summary>
        /// 加载表单 数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> LoadFormAsync(Guid Id)
        {
            var res = new Dictionary<string, object>();

            var Model = await this.FindByIdAsync(Id);

            res[nameof(Id)] = Id;
            res[nameof(Model)] = Model.NullSafe();

            return res;
        }


        #endregion

        #region 导出 Excel

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<byte[]> ExportExcel(Cms_Link Search)
        {
            var tableViewModel = await this.FindListAsync(1, 999999, Search);
            return this.ExportExcelByTableViewModel(tableViewModel);
        }

        #endregion

    }
}
