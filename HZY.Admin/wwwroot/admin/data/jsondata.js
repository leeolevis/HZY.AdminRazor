﻿var json_article_templateVal = {
    "article": "普通详情",
    "article_img": "图文详情",
    "article_job": "招聘详情",
};

var json_article_template =
    [{
        label: '普通详情',
        value: 'article'
    }, {
        label: '图文详情',
        value: 'article_img'
    }, {
        label: '招聘详情',
        value: 'article_job'
    }]
    ;

var json_classify_templateVal = {
    "list": "普通列表",
    "list_img": "图文列表",
    "list_job": "招聘列表",
};

var json_classify_template =
    [{
        label: '普通列表',
        value: 'list'
    }, {
        label: '图文列表',
        value: 'list_img'
    }, {
        label: '招聘列表',
        value: 'list_job'
    }]
    ;


var json_menu_typeVal = {
    "type": "父级菜单",
    "list": "关联分类",
    "view": "关联详情",
    "system": "系统内容",
    "link": "外部链接",
};

var json_menu_type =
    [{
        label: '父级菜单',
        value: 'type'
    },{
        label: '关联分类',
        value: 'list'
    }, {
        label: '关联详情',
        value: 'view'
    }, {
        label: '系统内容',
        value: 'system'
    }, {
        label: '外部链接',
        value: 'link'
    }]
    ;