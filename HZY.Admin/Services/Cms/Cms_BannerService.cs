﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Services.Cms
{
    using System.Threading.Tasks;
    using HZY.EFCore.Repository;
    using HZY.Models;
    using HZY.Models.Sys;
    using HZY.Toolkits;
    using HZY.EFCore.Base;
    using HZY.Admin.Services.Core;
    using HZY.EFCore;
    using HZY.Models.Cms;
    using Microsoft.AspNetCore.Http;

    public class Cms_BannerService : ServiceBase<Cms_Banner>
    {
        public Cms_BannerService(EFCoreContext _db)
            : base(_db)
        {

        }

        #region CURD 基础

        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<TableViewModel> FindListAsync(int Page, int Rows, Cms_Banner Search)
        {
            var query = this.Query()
                .WhereIF(w => w.Banner_Name.Contains(Search.Banner_Name), !string.IsNullOrWhiteSpace(Search?.Banner_Name))
                .OrderByDescending(w => w.Banner_Num)
                .Select(w => new
                {
                    w.Banner_Num,
                    w.Banner_Name,
                    w.Banner_Type,
                    w.Banner_Picture,
                    w.Banner_UrlID,
                    w.Banner_UrlVal,
                    w.Banner_IsShow,
                    w.Banner_IsDelete,
                    w.Banner_CreateTime,
                    w.Banner_ModifyTime,
                    w.Banner_ModifyBy,
                    _ukid = w.Banner_ID
                })
                ;

            return await this.db.AsTableViewModelAsync(query, Page, Rows, typeof(Cms_Banner));
        }

        /// <summary>
        /// 新增\修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> SaveAsync(Cms_Banner model, string webRootPath, IFormFile Photo)
        {
            if (Photo != null)
                model.Banner_Picture = this.HandleUploadImageFile(Photo, webRootPath);

            await this.InsertOrUpdateAsync(model);

            return model.Banner_ID;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(List<Guid> Ids)
            => await this.DeleteAsync(w => Ids.Contains(w.Banner_ID));

        /// <summary>
        /// 加载表单 数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> LoadFormAsync(Guid Id)
        {
            var res = new Dictionary<string, object>();

            var Model = await this.FindByIdAsync(Id);

            res[nameof(Id)] = Id;
            res[nameof(Model)] = Model.NullSafe();

            return res;
        }


        #endregion

        #region 导出 Excel

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<byte[]> ExportExcel(Cms_Banner Search)
        {
            var tableViewModel = await this.FindListAsync(1, 999999, Search);
            return this.ExportExcelByTableViewModel(tableViewModel);
        }

        #endregion

    }
}
