﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Admin.Services.Cms
{
    using System.Threading.Tasks;
    using HZY.EFCore.Repository;
    using HZY.Models;
    using HZY.Models.Sys;
    using HZY.Toolkits;
    using HZY.EFCore.Base;
    using HZY.Admin.Services.Core;
    using HZY.EFCore;
    using HZY.Models.Cms;
    using Microsoft.AspNetCore.Http;

    public class Cms_ArticleService : ServiceBase<Cms_Article>
    {
        public Cms_ArticleService(EFCoreContext _db)
            : base(_db)
        {

        }

        #region CURD 基础

        //递归子分类
        private void FindListClassId(Guid classId, List<Guid> classListId)
        {
            var listId = from c in db.Cms_Classifys
                         where c.Classify_ParentID == classId
                         select new { c.Classify_ID };

            var count = listId.Count();
            if (count >= 1)
            {
                var result = listId.ToList();
                foreach (var item in result)
                    classListId.Add(item.Classify_ID);

                foreach (var item in result)
                    FindListClassId(item.Classify_ID, classListId);
            }
        }

        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<TableViewModel> FindListAsync(int Page, int Rows, Cms_Article Search)
        {
            List<Guid> classListId = new List<Guid>();
            if (Search != null && Search.Article_ClassID.HasValue && Search.Article_ClassID != Guid.Empty)
            {
                classListId.Add(Search.Article_ClassID.Value);
                FindListClassId(Search.Article_ClassID.Value, classListId);
            }

            var query = this.Query()
                .Where(w => w.Article_ClassID != Guid.Empty)
                .WhereIF(w => classListId.Contains(w.Article_ClassID.Value), classListId.Count > 0)
                .WhereIF(w => w.Article_Title.Contains(Search.Article_Title) || w.Article_SubTitle.Contains(Search.Article_Title), !string.IsNullOrWhiteSpace(Search?.Article_Title))
                .OrderByDescending(w => w.Article_CreateTime)
                .OrderByDescending(w => w.Article_ModifyTime)
                .OrderByDescending(w => w.Article_IsTop)
                .Select(w => new
                {
                    w.Article_Title,
                    w.Article_Color,
                    w.Article_SubTitle,
                    w.Article_Picture,
                    w.Article_Document,
                    w.Article_Tag,
                    w.Article_Describe,
                    w.Article_Author,
                    w.Article_ClassID,
                    w.Article_Url,
                    w.Article_Templete,
                    w.Article_Content,
                    w.Article_IsTop,
                    w.Article_IsShow,
                    w.Article_IsReply,
                    w.Article_IsDelete,
                    w.Article_IsHomePage,
                    w.Article_CreateTime,
                    w.Article_ModifyTime,
                    w.Article_ModifyBy,
                    _ukid = w.Article_ID
                })
                ;

            return await this.db.AsTableViewModelAsync(query, Page, Rows, typeof(Cms_Article));
        }

        public async Task<TableViewModel> SystemListAsync(int Page, int Rows, Cms_Article Search)
        {
            var query = this.Query()
                .Where(w => w.Article_ClassID == Guid.Empty)
                .WhereIF(w => w.Article_Title.Contains(Search.Article_Title) , !string.IsNullOrWhiteSpace(Search?.Article_Title))
                .OrderByDescending(w => w.Article_CreateTime)
                .OrderByDescending(w => w.Article_ModifyTime)
                .OrderByDescending(w => w.Article_IsTop)
                .Select(w => new
                {
                    w.Article_Title,
                    w.Article_Color,
                    w.Article_SubTitle,
                    w.Article_Picture,
                    w.Article_Document,
                    w.Article_Tag,
                    w.Article_Describe,
                    w.Article_Author,
                    w.Article_ClassID,
                    w.Article_Url,
                    w.Article_Templete,
                    w.Article_Content,
                    w.Article_IsTop,
                    w.Article_IsShow,
                    w.Article_IsReply,
                    w.Article_IsDelete,
                    w.Article_IsHomePage,
                    w.Article_CreateTime,
                    w.Article_ModifyTime,
                    w.Article_ModifyBy,
                    _ukid = w.Article_ID
                })
                ;

            return await this.db.AsTableViewModelAsync(query, Page, Rows, typeof(Cms_Article));
        }

        /// <summary>
        /// 新增\修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> SaveAsync(Cms_Article model, string webRootPath, IFormFile Photo = null, IFormFile Document = null)
        {
            if (Photo != null)
                model.Article_Picture = this.HandleUploadImageFile(Photo, webRootPath);

            if (Document != null)
                model.Article_Document = this.HandleUploadFile(Document, webRootPath);

            await this.InsertOrUpdateAsync(model);

            return model.Article_ID;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(List<Guid> Ids)
            => await this.DeleteAsync(w => Ids.Contains(w.Article_ID));

        /// <summary>
        /// 加载表单 数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> LoadFormAsync(Guid Id)
        {
            var res = new Dictionary<string, object>();

            var Model = await this.FindByIdAsync(Id);

            var model = Model.NullSafe();
            if (string.IsNullOrEmpty(model.Article_Color)) model.Article_Color = "#000000";

            res[nameof(Id)] = Id;
            res[nameof(Model)] = model;

            return res;
        }

        /////// <summary>
        /////// 通过Id查找数据
        /////// </summary>
        /////// <param name="Id"></param>
        /////// <returns></returns>
        ////public async Task<Cms_Article> LoadByIdAsync(Guid Id)
        ////    => await this.FindByIdAsync(Id);


        #endregion

        #region 导出 Excel

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="Search"></param>
        /// <returns></returns>
        public async Task<byte[]> ExportExcel(Cms_Article Search)
        {
            var tableViewModel = await this.FindListAsync(1, 999999, Search);
            return this.ExportExcelByTableViewModel(tableViewModel);
        }

        #endregion

    }
}
