﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.Models.Cms
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Cms_Article))]
    public class Cms_Article
    {
        [Key]
        public Guid Article_ID { get; set; }
        /// <summary>
        /// 标题 => 备注:Article_Title 
        /// </summary>
        [Required(ErrorMessage = "标题不能为空!")]
        public string Article_Title { get; set; } = string.Empty;
        /// <summary>
        /// 标题颜色 => 备注:Article_Color 
        /// </summary>
        public string Article_Color { get; set; } = string.Empty;
        /// <summary>
        /// 副标题 => 备注:Article_SubTitle 
        /// </summary>
        public string Article_SubTitle { get; set; } = string.Empty;
        /// <summary>
        /// 图片 => 备注:Article_Picture 
        /// </summary>
        public string Article_Picture { get; set; } = string.Empty;
        /// <summary>
        /// 文档
        /// </summary>
        public string Article_Document { get; set; } = string.Empty;
        /// <summary>
        /// 标签 => 备注:Article_Tag 
        /// </summary>
        public string Article_Tag { get; set; } = string.Empty;
        /// <summary>
        /// 描述 => 备注:Article_Describe 
        /// </summary>
        public string Article_Describe { get; set; } = string.Empty;
        /// <summary>
        /// 作者 => 备注:Article_Author 
        /// </summary>
        public string Article_Author { get; set; } = string.Empty;
        /// <summary>
        /// 所属分类 => 备注:Article_ClassID 
        /// </summary>
        public Guid? Article_ClassID { get; set; } = Guid.Empty;
        /// <summary>
        /// 外链地址 => 备注:Article_Url 
        /// </summary>
        [Url(ErrorMessage = "链接地址不合法")]
        public string Article_Url { get; set; } = string.Empty;
        /// <summary>
        /// 模板 => 备注:Article_Templete 
        /// </summary>
        public string Article_Templete { get; set; } = string.Empty;
        /// <summary>
        /// 内容 => 备注:Article_Content 
        /// </summary>
        public string Article_Content { get; set; } = string.Empty;
        /// <summary>
        /// 是否置顶 => 备注:Article_IsTop 
        /// </summary>
        public int? Article_IsTop { get; set; } = 2;
        /// <summary>
        /// 是否显示 => 备注:Article_IsShow 
        /// </summary>
        public int? Article_IsShow { get; set; } = 1;
        /// <summary>
        /// 是否可回复 => 备注:Article_IsReply 
        /// </summary>
        public int? Article_IsReply { get; set; } = 2;
        /// <summary>
        /// 是否删除 => 备注:Article_IsDelete 
        /// </summary>
        public int? Article_IsDelete { get; set; } = 2;

        public int? Article_IsSystem { get; set; } = 2;

        /// <summary>
        /// 是否首页显示 => 备注:Article_IsHomePage 
        /// </summary>
        public int? Article_IsHomePage { get; set; } = 2;
        /// <summary>
        /// 创建时间
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Article_CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 修改时间 => 备注:Article_ModifyTime 
        /// </summary>
        public DateTime Article_ModifyTime { get; set; }
        /// <summary>
        /// 修改人 => 备注:Article_ModifyBy 
        /// </summary>
        public string Article_ModifyBy { get; set; } = string.Empty;

    }
}