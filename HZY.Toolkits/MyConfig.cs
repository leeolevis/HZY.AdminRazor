﻿using System;
using System.IO;
using System.Xml;

namespace HZY.Toolkits
{
    public class MyConfig
    {
        private static string XMLPath;
        public MyConfig(string filePath, string fileName = "MyConfig.Config")
        {
            XMLPath = Path.Combine(filePath, fileName);
        }

        private static readonly string[,] Nodes = new string[,] {
        {"BaseConfig", "配置文件" } };

        private static void WriteNode(XmlWriter writer, string ElementName, string AttributeValue)
        {
            writer.WriteStartElement(ElementName);
            writer.WriteAttributeString("Value", AttributeValue);
            writer.WriteEndElement();
        }

        public static void Create()
        {
            if (File.Exists(XMLPath))
            {
                XmlDocument document = new XmlDocument();
                try
                {
                    document.Load(XMLPath);
                }
                catch (System.UnauthorizedAccessException)
                {
                    throw new Exception("配置文件MyConfig.Config拒绝访问，请与管理员联系！");
                }

                XmlNode Root = document.SelectSingleNode("configuration");

                if (Root.ChildNodes.Count < Nodes.Length / 2)
                {
                    XmlNodeList NodesList = Root.ChildNodes;
                    for (int i = 0; i < Nodes.Length / 2; i++)
                    {
                        int j = 0;
                        foreach (XmlNode Node in NodesList)
                        {
                            if (Node.LocalName == Nodes[i, 0])
                            {
                                break;
                            }
                            j++;
                        }
                        if (j == NodesList.Count)
                        {
                            System.Xml.XPath.XPathNavigator Nav = Root.CreateNavigator();
                            Nav.AppendChild("<" + Nodes[i, 0] + " Value=\"" + Nodes[i, 1] + "\" />");
                        }
                    }
                    document.Save(XMLPath);
                }
            }
            else
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(XMLPath, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("configuration");

                    for (int i = 0; i < Nodes.Length / 2; i++)
                    {
                        WriteNode(writer, Nodes[i, 0], Nodes[i, 1]);
                    }

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }
        }

        public string GetValue(string NodeName)
        {
            Create();

            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(XMLPath);
            }
            catch (System.UnauthorizedAccessException)
            {
                throw new Exception("配置文件MyConfig.Config拒绝访问，请与管理员联系！");
            }

            XmlNode node = document.SelectSingleNode("configuration/" + NodeName);
            return node.Attributes["Value"].Value;
        }

        public void SetValue(string NodeName, string NewValue)
        {
            Create();

            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(XMLPath);
            }
            catch (System.UnauthorizedAccessException)
            {
                throw new Exception("配置文件MyConfig.Config拒绝访问，请与管理员联系！");
            }

            XmlNode node = document.SelectSingleNode("configuration/" + NodeName);
            node.Attributes["Value"].Value = NewValue;

            document.Save(XMLPath);
        }
    }
}
