﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.Models.Cms
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Cms_Classify))]
    public class Cms_Classify
    {
        [Key]
        public Guid Classify_ID { get; set; } = Guid.Empty;

        [Required(ErrorMessage = "排序号不能为空!")]
        [RegularExpression(@"^[0-9]*$")]
        public int Classify_Num { get; set; } = 1;

        /// <summary>
        /// 分类类别 文章等不同类别 => 备注:Classify_TYPE
        /// </summary>
        public string Classify_Type { get; set; } = string.Empty;

        /// <summary>
        /// 分类名称 => 备注:Classify_NAME
        /// </summary>
        [Required(ErrorMessage = "分类名称不能为空!")]
        public string Classify_Name { get; set; } = string.Empty;

        /// <summary>
        /// 分类父ID => 备注:Classify_ParentID
        /// </summary>
        public Guid Classify_ParentID { get; set; } = Guid.Empty;

        /// <summary>
        /// 分类模板 => 备注:Classify_TEMPLETE
        /// </summary>
        [Required(ErrorMessage = "分类模板不能为空!")]
        public string Classify_Templete { get; set; } = string.Empty;

        /// <summary>
        /// URL => 备注:Classify_URL
        /// </summary>
        public string Classify_Url { get; set; } = string.Empty;

        /// <summary>
        /// 文章ID => 备注:Classify_ARTICLEID
        /// </summary>
        public Guid Classify_ArticleID { get; set; } = Guid.Empty;


        public int Classify_IsShow { get; set; } = 1;

        public int Classify_IsDelete { get; set; } = 2;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Classify_CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 修改时间 => 备注:Classify_ModifyTime
        /// </summary>
        public DateTime Classify_ModifyTime { get; set; }

        /// <summary>
        /// 修改人 => 备注:Classify_ModifyBy
        /// </summary>
        public string Classify_ModifyBy { get; set; } = string.Empty;

    }
}